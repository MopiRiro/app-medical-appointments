import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-welcome-view',
  templateUrl: './welcome-view.component.html',
  styleUrls: ['./welcome-view.component.scss']
})
export class WelcomeViewComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  ExploreMore(){
    this.router.navigate(['login']);
  }

}
