import { Component, OnInit, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'app-create-medical-appointments',
  templateUrl: './create-medical-appointments.component.html',
  styleUrls: ['./create-medical-appointments.component.scss']
})
export class CreateMedicalAppointmentsComponent implements OnInit {
  name:string= '';
  date:string = '';
  time:string = '';
  specialty:string = '';
  symptom:string = '';

  //Se utiliza para enviar una alerta cuando algun campo del formulario no esta lleno
  wrongForm = false;
  @Output() newAppointment = new EventEmitter<any>();

  constructor() { }

  ngOnInit(): void {
  }

  addAppointments() {
    if(this.name == '' || this.date == '' || this.time== '' || this.symptom == '' || this.specialty == '') {
      this.wrongForm = true;
      return;
    }
    this.wrongForm = false;
    

    //Creamos objeto para enviarselo al padre 
    const appointment = {
      name: this.name,
      date: this.date,
      time: this.time,
      specialty:this.specialty,
      symptom: this.symptom,
      
    }
    console.log(appointment);
    this.newAppointment.emit(appointment);
    this.resetFields();
    
  }

  resetFields() {
    this.name = '',
    this.date = '',
    this.time = '',
    this.specialty = '',
    this.symptom = ''    
  }
  

}
