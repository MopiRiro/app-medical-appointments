import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateMedicalAppointmentsComponent } from './create-medical-appointments.component';

describe('CreateMedicalAppointmentsComponent', () => {
  let component: CreateMedicalAppointmentsComponent;
  let fixture: ComponentFixture<CreateMedicalAppointmentsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateMedicalAppointmentsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateMedicalAppointmentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
