import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-medical-appointment-list',
  templateUrl: './medical-appointment-list.component.html',
  styleUrls: ['./medical-appointment-list.component.scss']
})
export class MedicalAppointmentListComponent implements OnInit {
  nameUpdate:string = '';
  dateUpdate:string = '';
  timeUpdate:string = '';
  imcUpdate:number =0;
  specialtyUpdate:string = '';
  symptomUpdate:string = '';

  @Input() appointmentsList:any;
  @Output() deleteAppointments = new EventEmitter<number>();
  constructor() { }

  ngOnInit(): void {
  }

  deleteAppointment(index:number){
    this.deleteAppointments.emit(index);
  }

  updateAppointment(index:number) {
    
  }

}
