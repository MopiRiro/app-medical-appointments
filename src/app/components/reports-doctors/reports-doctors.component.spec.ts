import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportsDoctorsComponent } from './reports-doctors.component';

describe('ReportsDoctorsComponent', () => {
  let component: ReportsDoctorsComponent;
  let fixture: ComponentFixture<ReportsDoctorsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReportsDoctorsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportsDoctorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
