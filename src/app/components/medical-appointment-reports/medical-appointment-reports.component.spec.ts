import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MedicalAppointmentReportsComponent } from './medical-appointment-reports.component';

describe('MedicalAppointmentReportsComponent', () => {
  let component: MedicalAppointmentReportsComponent;
  let fixture: ComponentFixture<MedicalAppointmentReportsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MedicalAppointmentReportsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MedicalAppointmentReportsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
