import { Component, OnInit } from '@angular/core';
import { faBookBookmark, faGlobe, faInbox } from '@fortawesome/free-solid-svg-icons';
@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
  
  faCoffee = faGlobe;
  faFb=faInbox;
  faIg=faBookBookmark;

}
