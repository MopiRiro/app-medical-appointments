import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  user:string = '';
  password:string = '';

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  Login(){
    if (this.user == 'monica@gmail.com' || this.password == '123456'){
      this.router.navigate(['bmicalculator']);
    } else if(this.user == '' || this.password == ''){
      Swal.fire({
        icon: 'error',
        title: 'Ups...',
        confirmButtonColor:'#437F97',
        text: 'Ingrese datos de usuario porfavor'
      })
    }
    
  }

}
