import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-result-bmi-calculator',
  templateUrl: './result-bmi-calculator.component.html',
  styleUrls: ['./result-bmi-calculator.component.scss']
})
export class ResultBmiCalculatorComponent implements OnInit {
  bmi:number = 0;
  resultado:string = '';
  interpretacion:string = '';
  constructor(private route: ActivatedRoute) {
    // console.log(route.snapshot.paramMap.get('valor'));
    this.bmi = +route.snapshot.paramMap.get('valor')!;
  }

  ngOnInit(): void {
    this.getResultado();
  }
  getResultado() {

    if(this.bmi >=25) {
      this.resultado = 'Exceso de peso';
      this.interpretacion = 'Tienes un peso corporal superior al normal'
    } else if(this.bmi >= 18.5) {
      this.resultado = 'Normal';
      this.interpretacion = 'Tienes un peso corporal normal'
    } else {
      this.resultado = 'Bajo peso';
      this.interpretacion = 'Tienes un peso corporal más bajo de lo normal'
    }
  }
}
