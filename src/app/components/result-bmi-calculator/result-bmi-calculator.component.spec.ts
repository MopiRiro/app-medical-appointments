import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResultBmiCalculatorComponent } from './result-bmi-calculator.component';

describe('ResultBmiCalculatorComponent', () => {
  let component: ResultBmiCalculatorComponent;
  let fixture: ComponentFixture<ResultBmiCalculatorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ResultBmiCalculatorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ResultBmiCalculatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
