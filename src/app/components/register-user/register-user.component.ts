import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-register-user',
  templateUrl: './register-user.component.html',
  styleUrls: ['./register-user.component.scss']
})
export class RegisterUserComponent implements OnInit {
  email:string = '';
  user:string = '';
  password:string = '';

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  GoToLogin(){
    this.router.navigate(['login']);
  }

  UserRegister(){
    Swal.fire({
      position: 'center',
      icon: 'success',
      title: 'Usuario Registrado',
      text: 'Por favor inicia sesión',
      showConfirmButton: false,
      timer: 2500
    })
    this.router.navigate(['login']);
  //   if(this.email == '' ||this.user == '' || this.password == ''){
  //     Swal.fire({
  //       icon: 'error',
  //       title: 'Ups...',
  //       confirmButtonColor:'#437F97',
  //       text: 'Ingrese datos para su registro'
  //     })
  //   } else {
  //   Swal.fire({
  //     position: 'center',
  //     icon: 'success',
  //     title: 'Usuario Registrado',
  //     text: 'Por favor inicia sesión',
  //     showConfirmButton: false,
  //     timer: 1500
  //   })
  //   this.router.navigate(['login']);
  // }
  }

}
