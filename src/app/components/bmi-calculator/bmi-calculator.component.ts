import { Component, OnInit } from '@angular/core';
import { faMars, faMinus, faPlus, faVenus } from '@fortawesome/free-solid-svg-icons';
import { Router } from '@angular/router';
@Component({
  selector: 'app-bmi-calculator',
  templateUrl: './bmi-calculator.component.html',
  styleUrls: ['./bmi-calculator.component.scss']
})
export class BmiCalculatorComponent implements OnInit {
  edad:number = 20;
  peso:number = 60;
  altura:number = 180;
  genero:string = '';

  faMars = faMars;
  faVenus = faVenus;
  faMinus = faMinus;
  faPlus = faPlus;

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  changeHeight(event:any){
    console.log(event.target.value);
    this.altura = event.target.value;
  }

  genderMars(){
    this.genero = 'Masculino';
  }

  genderVenus(){
    this.genero = 'Femenino';
  }

  calculate(){
    const BMI = this.peso / Math.pow(this.altura/100, 2);
    this.router.navigate(['/createappointments', BMI.toFixed(1)])
  }

}
