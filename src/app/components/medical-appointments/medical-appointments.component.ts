import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-medical-appointments',
  templateUrl: './medical-appointments.component.html',
  styleUrls: ['./medical-appointments.component.scss']
})
export class MedicalAppointmentsComponent implements OnInit {
  imc:number =0;

  constructor(private route: ActivatedRoute) { 
    this.imc = +route.snapshot.paramMap.get('valor')!;
  }

  ngOnInit(): void {
    
    
  }

  listAppointments: any[] = [];

  addAppointments(appointment:any){
    console.log('father');
    console.log(appointment);
    this.listAppointments.push(appointment);
    console.log(this.listAppointments);
  }

  deleteAppointmentsListed(index:number){
    this.listAppointments.splice(index, 1);
  }

}
