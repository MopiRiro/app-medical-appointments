import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MedicalAppointmentListComponent } from './components/medical-appointment-list/medical-appointment-list.component';
import { CreateMedicalAppointmentsComponent } from './components/create-medical-appointments/create-medical-appointments.component';
import { LoginComponent } from './components/login/login.component';
import { NavBarComponent } from './components/nav-bar/nav-bar.component';
import { MedicalAppointmentsComponent } from './components/medical-appointments/medical-appointments.component';
import { WelcomeViewComponent } from './components/welcome-view/welcome-view.component';
import { FooterComponent } from './components/footer/footer.component';
import { BmiCalculatorComponent } from './components/bmi-calculator/bmi-calculator.component';
import { MedicalAppointmentReportsComponent } from './components/medical-appointment-reports/medical-appointment-reports.component';
import { ResultBmiCalculatorComponent } from './components/result-bmi-calculator/result-bmi-calculator.component';
import { RegisterUserComponent } from './components/register-user/register-user.component';
import { HealthWorkersComponent } from './components/health-workers/health-workers.component';
import { AdministratorComponent } from './components/administrator/administrator.component';
import { ReportsDoctorsComponent } from './components/reports-doctors/reports-doctors.component';
import { PatientsComponent } from './components/patients/patients.component';


@NgModule({
  declarations: [
    AppComponent,
    MedicalAppointmentListComponent,
    CreateMedicalAppointmentsComponent,
    LoginComponent,
    NavBarComponent,
    MedicalAppointmentsComponent,
    WelcomeViewComponent,
    FooterComponent,
    BmiCalculatorComponent,
    MedicalAppointmentReportsComponent,
    ResultBmiCalculatorComponent,
    RegisterUserComponent,
    HealthWorkersComponent,
    AdministratorComponent,
    ReportsDoctorsComponent,
    PatientsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    FontAwesomeModule,
    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
