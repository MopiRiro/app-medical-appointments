import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdministratorComponent } from './components/administrator/administrator.component';
import { BmiCalculatorComponent } from './components/bmi-calculator/bmi-calculator.component';
import { CreateMedicalAppointmentsComponent } from './components/create-medical-appointments/create-medical-appointments.component';
import { HealthWorkersComponent } from './components/health-workers/health-workers.component';
import { LoginComponent } from './components/login/login.component';
import { MedicalAppointmentReportsComponent } from './components/medical-appointment-reports/medical-appointment-reports.component';
import { MedicalAppointmentsComponent } from './components/medical-appointments/medical-appointments.component';
import { PatientsComponent } from './components/patients/patients.component';
import { RegisterUserComponent } from './components/register-user/register-user.component';
import { ReportsDoctorsComponent } from './components/reports-doctors/reports-doctors.component';
import { ResultBmiCalculatorComponent } from './components/result-bmi-calculator/result-bmi-calculator.component';
import { WelcomeViewComponent } from './components/welcome-view/welcome-view.component';

const routes: Routes = [
  {
    path:'',
    redirectTo:'/welcome',
    pathMatch:'full'
  },
  {
    path:'login',
    component:LoginComponent
  },
  {
    path:'createappointments/:valor',
    component:MedicalAppointmentsComponent
  },
  {
    path:'welcome',
    component:WelcomeViewComponent
  },
  {
    path:'reports',
    component:MedicalAppointmentReportsComponent
  },
  {
    path:'bmicalculator',
    component:BmiCalculatorComponent
  },
  {
    path:'resultBmicalculator/:valor',
    component:ResultBmiCalculatorComponent 
  },
  {
    path:'registerUser',
    component:RegisterUserComponent 
  },
  {
    path:'administrador/doctor',
    component:HealthWorkersComponent
  },
  {
    path:'administrador',
    component:AdministratorComponent
  },
  {
    path:'reportes-doctor',
    component:ReportsDoctorsComponent
  },
  {
    path:'pacientes',
    component:PatientsComponent
  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
